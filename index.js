import Puppeteer from "puppeteer";
import fs from "fs";
import {PASSWORD, USERNAME, PATHDOWNLOAD} from "./config.js";

let password = PASSWORD;
let username = USERNAME;
let pathDownload = PATHDOWNLOAD;

(async () => {
    let linksToDownload = [];
    const browser = await Puppeteer.launch({headless: true});
    const page = await browser.newPage();
    await page.goto("https://uptobox.com/login?referer=homepage");
    await page.target().createCDPSession().then((client) => {
        return client.send('Page.setDownloadBehavior', {behavior: 'allow', downloadPath: pathDownload})
    });
    let data = fs.readFileSync('listing.txt', 'utf8');
    linksToDownload = data.split("\n");

    let nbDownloadPack = Math.ceil(linksToDownload.length / 5);
    console.log(`Il y aura ${nbDownloadPack} boucles fichiers à télécharger`);
    console.log(`Pour un total ${linksToDownload.length} liens à télécharger...`);

    console.log('Connexion sur uptobox...');
    //Username
    await page.waitForSelector('input[name=login]');
    await page.waitForTimeout(1000);
    await page.$eval('input[name=login]', (el, username) => {
        el.value = username
    }, username);

    //Password
    await page.waitForSelector('input[name=password]');
    await page.waitForTimeout(1000);
    await page.$eval('input[name=password]', (el, password) => {
        el.value = password
    }, password);

    await page.waitForTimeout(2000);
    await page.click('button.login');

    console.log('Connexion réussi');
    await page.waitForTimeout(5000);

    let i = 0;

    for(let j = 0; j < nbDownloadPack; j++) {
        console.log(`Pack numéro ${j+1} ...`);
        await downloadFiveFiles(page, i, linksToDownload);
        console.log('On patiente 5 min pour le traitement des ces téléchargements');
        i += 5;
        await page.waitForTimeout(60000);
    }

    await page.waitForTimeout(13000);

    //await browser.close();
})();

async function downloadFiveFiles(page, currentI, linksToDownload)
{
    for(let i=currentI; i < currentI+5; i++) {
        if(linksToDownload[i] !== undefined) {
            console.log(`Changement de page vers le fichier numéro ${i+1}....`);
            await page.goto(linksToDownload[i]);
            await page.waitForTimeout(3000);
            try {
                await page.waitForSelector('.big-button-green-flat');
                console.log('Lancement du téléchargement');
                await page.click(".big-button-green-flat");
            } catch (e) {
                console.log(e);
                console.log('Bouton de téléchargement introuvable pour le lien '+linksToDownload[i]);
            }
        }
    }
}